# Mise en place d'un réseau d'entreprise



## Partie I : Mise en place du réseau sur les VMs 

### Etape 1.1 - Réseau privé de l'entreprise

Par la suite, LAN désignera le réseau privé de l'entreprise.

Exemple de plan d'adressage d'une LAN :

* Réseau : 192.168.8.0/24 :
  * Client 1 : 192.168.8.3,
  * Client 2 : 192.168.8.4. 

### Etape 1.2 - Réseau public de l'entreprise

Par la suite, DMZ désignera le réseau publique de l'entreprise.

Exemple de plan d'adressage d'une DMZ :

* Réseau : 195.10.3.0/24 :
  * Web server : 195.10.3.3,
  * DNS : 195.10.3.4.

### Etape 1.3 - Interconnexion des réseaux de l'entreprise

#### 	Question 1.1

Un NAT n'est pas nécessaire pour la communication entre la LAN et la DMZ. En effet, il suffit d'ajouter des routes dans le routeur pour qu'ils puissent communiquer. Le prêt de l'adresse publique du routeur n'est donc pas nécessaire.

#### 	Question 1.2

![image-20191213165925026](./src/circulation.png)

​						s : source					d : destination

***Quelles sont les étapes de la communication ?***

La communication se passe en 4 étapes :

1. Le client envoi une requête vers le serveur, 
2. Le message est réceptionné par le routeur, qui consulte sa table de routage et le fait suivre vers la DMZ,
3. Le serveur réceptionne le message, le traite puis envoi sa réponse à l'adresse du client,
4. Le message est réceptionné par le routeur, qui consulte sa table de routage et le fait suivre au client.

***Que manque-t'il par rapport à un cas classique ?***

La différence avec un cas classique est le prêt d'adresse publique par le routeur (NAT), qui n'est pas fait ici. En effet, les machines de la LAN accèdent aux machines de la DMZ avec des adresses privées, ce qui n'est pas possible dans la réalité.  

#### 	Question 1.3

***Route d'une machine du réseau privé :***

| Destination  | Masque | Passerelle   | Interface | Description                           |
| ------------ | ------ | ------------ | --------- | ------------------------------------- |
| 0.0.0.0      | /0     | 192.168.56.1 | enp0s3    | passerelle par défaut vers le routeur |
| 192.168.56.0 | /24    | 192.168.56.5 | enp0s3    | route vers la LAN                     |

***Routes du serveur de la DMZ :***

| Destination | Masque | Passerelle | Interface | Description                           |
| ----------- | ------ | ---------- | --------- | ------------------------------------- |
| 0.0.0.0     | /0     | 195.10.4.1 | enp0s3    | passerelle par défaut vers le routeur |
| 195.10.4.0  | /24    | 195.10.4.2 | enp0s3    | route vers la DMZ                     |

***Routes du routeur :***

| Destination  | Masque | Passerelle | Interface    | Description                                      |
| ------------ | ------ | ---------- | ------------ | ------------------------------------------------ |
| 192.168.56.0 | /24    | enp0s3     | 192.168.56.1 | route vers la LAN                                |
| 195.10.4.0   | /24    | enp0s8     | 195.10.4.1   | route vers la DMZ                                |
| 200.2.2.0    | /24    | enp0s9     | 200.2.2.4    | route vers l'extérieur du réseau de l'entreprise |

## Partie II : Interconnexion avec le "reste du monde"

Par la suite, WAN désignera le monde extérieur.

### Etape 2.1 - Mise en place d'un routeur vers l'extérieur

#### 	Question 2.1

Il faut d'abord ajouter une nouvelle interface sur chaque routeur, avec une adresse publique.

Il faut ensuite ajouter une nouvelle route par machine. Il s'agit de la gateway, qui prend alors l'adresse publique du routeur. 

#### 	Question 2.2

A cette étape, la LAN ne communique toujours pas avec le reste du monde. En effet, pour avoir le droit de circuler sur le WAN, il lui faut une adresse publique. Elle ne lui sera attribuée qu'après configuration du NAT sur le routeur.

## Partie III : Communication du réseau privé avec les serveurs web

### Etape 3.1 - Réseau privé

Commande qui indique au routeur de prêter son adresse quand un message arrive de la LAN et va vers du WAN.

```
iptables -t nat -A POSTROUTING -s <adresse LAN> -o <interface WAN> -j MASQUERADE
```

Pour faire persister cette configuration lors du redémarrage des machines, nous avons utilisé le package `iptables-persitent`.

#### 	Question 3.1

Quand on envoi un message depuis une machine avec une adresse privé, le routeur prête son adresse publique comme source du message. Il possède une table interne qui lui permet d'associer un port à chaque machine pour savoir à laquelle appartient le message lors de la réception de la réponse.

#### 	Question 3.2 

Tout le monde peut communiquer avec tout le monde sauf les LAN entre elles. En effet, nous n'avons pas précisé de routes menant vers les réseau privé dans le routeur.

Schéma de notre architecture réseau :

<img src="./src/dessin.png" alt="./src/dessin.png" style="zoom:50%;" />



## Partie IV : Mise en place de la sécurité

### Etape 4.1 - Politique de sécurité

Nous avons choisi une politique de sécurité classique. Nous laissons toutes les règles existantes et rajoutons une sécurité dans la LAN. Seuls les messages réponse sont autorisés à y pénétrer.

### Etape 4.2 - Mise en place de la politique choisie

#### 	Question 4.1

On autorise la communication vers la DMZ uniquement sur les ports de notre serveur web et de notre serveur DNS.

Pour la LAN personne ne peut y accéder. Seul la LAN peut initier les communications.

Les requêtes de l'extérieur ne sont autorisées à pénétrer que si elles sont une réponse à une requête d'une des machines de la LAN.

#### 	Question 4.2

L'état `TCP` nous permet de savoir si un message résulte de l'établissement d'une communication ou si il s'agit de la réponse à une requête. Ces états nous permettent de n'autoriser que ce type de message à entrer dans notre LAN.



## Partie V : Configuration automatique

### Etape 5.1 -  DHCP sur réseau privé 

#### 		Question 5.1

Les informations à renseigner dans le DHCP sont :

- l'adresse réseau,
- son masque,
- l'interface de la LAN,
- l'intervalle d'adresses disponibles pour la LAN,
- l'adresse de diffusion du réseau,
- le temps d'affectation d'une adresse,
- le DNS,
- la gateway.

Ces mêmes informations seront véhiculées sur chaque machine configuré par le DHCP, à l'exception de l'intervalle des adresses disponibles (on n'affecte qu'une adresse à une machine). 

#### 	Question 5.2

DHCP est un protocole permettant l'adressage dynamique des clients d'un réseau. A chaque machine il peut associer une adresse IP, un masque réseau et un DNS. Les adresses IP sont tirées d'un intervalle (un sous-réseau) préalablement configuré. Elles ne sont attribuées que pour un certain temps, au bout duquel elles sont de nouveau disponibles pour d'autre machines du réseau.  

#### 	Question 5.3

Un serveur DHCP pose un danger de par l’absence d'identification formelle, à la fois du serveur et du client. En effet, une machine pourrait se faire passer pour un serveur DHCP et délivrer de mauvaises informations sur le réseau. Inversement, une machine peut faire croire qu'elle appartient au réseau du DHCP. En changeant ses identifiants à répétition, la machine peut épuiser l'intervalle d'adresses du DHCP. Le réseau est alors bloqué.

## Partie VI : Mise en place d'un proxy web

Nous avons mis le proxy sur le routeur pour ne pas trop alourdir notre architecture. Dans un vrai réseau, nous aurions une machine serveur dans la LAN dédiée au DHCP et au proxy.

### Etape 6.1 - Proxy cache web

### Etape 6.2 - Proxy cache transparent

#### 	Question 6.1

Un proxy est un serveur qui se place entre la LAN et le monde extérieur. Ainsi toutes les requêtes envoyées depuis la LAN passent d'abord par le proxy. Cela permet de configurer des règles, nottament d'interdire l'accès à certains sites.

#### 	Question 6.2

L'utilisation d'un proxy transparent est bien plus confortable que celle d'un proxy explicite. En effet, il n'y a pas de configuration supplémentaire à faire lorsque l'on branche une machine au réseau ou que l'on change de navigateur.

#### 	Question 6.3

Un proxy transparent est plus compliqué d'un point de vue réseau car il implique de rediriger toutes les requêtes `http` et `https` vers le proxy. Pour cela nous avons utilisés les commandes iptables. En prerouting on redirige tous les messages provenant de l'interface de notre LAN du port 443 ou 80 vers le port de notre proxy, c'est-à-dire 3128.

## Partie VII : DNS

### Étape 7.1 - Espace de nommage

Voici l'espace de nommage que nous avons choisi :

![file:///home/n7student/Downloads/nameSpace(1).png](./src/nameSpace1.png)



### Etape 7.2 - Serveur DNS

Notre DNS n'est pas récursif car nous voulons juste communiquer entre les réseaux "lacoste", "hou" et "blasius".

### Etape 7.3 - Service global

Pour mettre en place notre service de DNS global inter-réseaux, nous avons mis en place des zones DNS en type **Slave**, en plus des zones **Master** que nous avions déjà mises en place sur nos réseaux respectifs.
Ces zones "slaves" ont pour "masters" les DNS des autres réseaux. Grâce à cela lorsqu'un réseau ne connait pas un nom de domaine, il peut demander à ses "masters" (au DNS des autres réseaux) la correspondance.

*Exemple : Le réseau Lacoste possède une zone slave avec comme master le DNS du réseau Hou et une autre avec pour master le DNS du réseau Blasius.*

En configurant le DNS dans notre serveur DHCP, il est alors affecté à toutes les machines de la LAN automatiquement.
